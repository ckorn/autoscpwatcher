﻿using CrossCutting.DataStoring.Contract.DataClasses;
using Logic.AutoScpManagement.Contract;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UI.AutoScpWatcher.Avalonia.Models
{
    public class DirectoryEntryModel : ReactiveObject
    {
        public string TabName => directoryEntry.TabName;
        public string Info { get; } = "";
        public List<SyncDirectoryInfo> SyncDirectoryInfoList => directoryEntry.SyncDirectoryInfoList;
        public string Log { get => log; set => IReactiveObjectExtensions.RaiseAndSetIfChanged(this, ref log, value); }
        private string log = string.Empty;
        public bool GuiEnabled { get => guiEnabled; set => IReactiveObjectExtensions.RaiseAndSetIfChanged(this, ref guiEnabled, value); }
        private bool guiEnabled = true;
        private readonly IAutoScpWatcher autoScpWatcher = new Logic.AutoScpManagement.AutoScpWatcher();
        private readonly DirectoryEntry directoryEntry;
        private readonly Queue<string> logQueue = new(5);
        private readonly CancellationTokenSource cancellationTokenSource = new();

        public DirectoryEntryModel(DirectoryEntry directoryEntry)
        {
            this.directoryEntry = directoryEntry;
            this.Info = $"{directoryEntry.Username}@{directoryEntry.Hostname}:{directoryEntry.Port} | {directoryEntry.IntervallSeconds} s";
            if (directoryEntry.PlaySound)
            {
                this.Info += " | Beep";
            }
            autoScpWatcher.FileTransfered += AutoScpWatcher_FileTransfered;
            autoScpWatcher.Log += AutoScpWatcher_Log;
        }
        private void AutoScpWatcher_Log(object? sender, string e)
        {
            while (logQueue.Count >= 5)
            {
                logQueue.Dequeue();
            }
            logQueue.Enqueue($"{DateTime.Now:G}: {e}");
            this.Log = string.Join(Environment.NewLine, logQueue);
        }

        private void AutoScpWatcher_FileTransfered(object? sender, EventArgs e)
        {
            if (this.directoryEntry.PlaySound)
            {
                Console.Beep();
            }
        }

        public void Start()
        {
            GuiEnabled = false;
            Task.Run(async () => await autoScpWatcher.Start(directoryEntry, cancellationTokenSource.Token))
                .ContinueWith((t =>
                {
                    if (t.Exception == null)
                    {
                        GuiEnabled = true;
                    }
                    else
                    {
                        AutoScpWatcher_Log(this, t.Exception.Message);
                    }
                }), TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
