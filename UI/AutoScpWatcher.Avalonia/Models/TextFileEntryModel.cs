﻿using CrossCutting.DataStoring.Contract.DataClasses;
using Logic.AutoScpManagement.Contract;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UI.AutoScpWatcher.Avalonia.Models
{
    public class TextFileEntryModel : ReactiveObject
    {
        public string TabName => textFileEntry.TabName;
        public string Info { get => info; set => IReactiveObjectExtensions.RaiseAndSetIfChanged(this, ref info, value); }
        private string info = string.Empty;
        public IReadOnlyList<TextFileLine> ContentLines { get => contentLines; set => IReactiveObjectExtensions.RaiseAndSetIfChanged(this, ref contentLines, value); }
        private IReadOnlyList<TextFileLine> contentLines = new List<TextFileLine>();
        public bool GuiEnabled { get => guiEnabled; set => IReactiveObjectExtensions.RaiseAndSetIfChanged(this, ref guiEnabled, value); }
        private bool guiEnabled = true;
        private string log = string.Empty;
        public bool Diff => textFileEntry.ShowDiff;
        private readonly IAutoScpWatcher autoScpWatcher = new Logic.AutoScpManagement.AutoScpWatcher();
        private readonly TextFileEntry textFileEntry;
        private readonly CancellationTokenSource cancellationTokenSource = new();

        public TextFileEntryModel(TextFileEntry textFileEntry)
        {
            this.textFileEntry = textFileEntry;
            SetInfoText();
            autoScpWatcher.FileTransfered += AutoScpWatcher_FileTransfered;
            autoScpWatcher.Log += AutoScpWatcher_Log;
            autoScpWatcher.TextFileLines += AutoScpWatcher_TextFileLines;
        }

        private void SetInfoText()
        {
            List<string> infoLines = new ();
            infoLines.Add($"{textFileEntry.Username}@{textFileEntry.Hostname}:{textFileEntry.Port}");
            infoLines.Add($"{textFileEntry.RemotePath}");
            infoLines.Add($"{textFileEntry.IntervallSeconds} s");
            if (textFileEntry.PlaySound)
            {
                infoLines.Add("Beep");
            }
            if (textFileEntry.ShowDiff)
            {
                infoLines.Add("Diff");
            }
            if (!string.IsNullOrEmpty(log))
            {
                infoLines.Add(log);
            }
            this.Info = string.Join(" | ", infoLines);
        }

        public void SetLog(string log) 
        {
            this.log = log;
            SetInfoText();
        }

        private void AutoScpWatcher_TextFileLines(object? sender, IReadOnlyList<TextFileLine> e)
        {
            this.ContentLines = e;
        }

        private void AutoScpWatcher_Log(object? sender, string e)
        {
            this.log = e;
            SetInfoText();
        }

        private void AutoScpWatcher_FileTransfered(object? sender, EventArgs e)
        {
            if (textFileEntry.PlaySound)
            {
                Console.Beep();
            }
        }

        public void Start()
        {
            GuiEnabled = false;
            Task.Run(async () => await autoScpWatcher.Start(textFileEntry, cancellationTokenSource.Token))
                .ContinueWith((t =>
                {
                    if (t.Exception == null)
                    {
                        GuiEnabled = true;
                    }
                    else
                    {
                        AutoScpWatcher_Log(this, t.Exception.Message);
                    }
                }), TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
