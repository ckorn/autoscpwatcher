using CrossCutting.DataStoring.Contract.DataClasses;
using Logic.AutoScpManagement.Contract;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UI.AutoScpWatcher.Avalonia.Models;

namespace UI.AutoScpWatcher.Avalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {       
        public List<object> TabList { get; set; } = new List<object>();

        private readonly Arguments? arguments = null;

        public MainWindowViewModel()
        {
            if (Program.IsApplicationRunning)
            {
                if (Program.ProgramArgs.Length > 0)
                {
                    this.arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<Arguments>(File.ReadAllText(Program.ProgramArgs[0])) ?? throw new Exception();
                    TabList.AddRange(this.arguments.Directories.Select(x => new DirectoryEntryModel(x)));
                    TabList.AddRange(this.arguments.TextFiles.Select(x => new TextFileEntryModel(x)));
                }
                else
                {
                    throw new InvalidOperationException("Please provide settings.json as parameter");
                }
            }
        }
    }
}
