using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UI.AutoScpWatcher.Avalonia.Models;

namespace UI.AutoScpWatcher.Avalonia.Views
{
    public partial class TextEditorUserControl : UserControl
    {
        private TextFileEntryModel? textFileEntryModel = null;
        private DiffColorizingTransformer? diffColorizingTransformer = null;
        public TextEditorUserControl()
        {
            InitializeComponent();

            this.DataContextChanged += TextEditorUserControl_DataContextChanged;
        }

        private void TextEditorUserControl_DataContextChanged(object? sender, System.EventArgs e)
        {
            if (textFileEntryModel != null)
            {
                textFileEntryModel.PropertyChanged -= TextFileEntryModel_PropertyChanged;
            }
            textFileEntryModel = (TextFileEntryModel?)this.DataContext;
            if (textFileEntryModel != null)
            {
                textFileEntryModel.PropertyChanged += TextFileEntryModel_PropertyChanged;
                if (textFileEntryModel.Diff)
                {
                    this.diffColorizingTransformer = new()
                    {
                        textFileEntryModel = textFileEntryModel
                    };

                    textEditor.TextArea.TextView.LineTransformers.Add(diffColorizingTransformer);
                }
                SetText();
            }
        }

        private void TextFileEntryModel_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TextFileEntryModel.ContentLines))
            {
                SetText();
            }
        }

        private void SetText()
        {
            void setText()
            {
                this.textEditor.Text = string.Join(Environment.NewLine, this.textFileEntryModel?.ContentLines
                    ?.Select(x => x.Text) ?? new List<string>());
            }
            if (this.CheckAccess())
            {
                setText();
            }
            else
            {
                Dispatcher.UIThread.Post(setText);
            }
        }
    }
}
