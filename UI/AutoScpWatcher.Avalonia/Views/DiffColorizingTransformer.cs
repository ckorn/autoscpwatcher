﻿using Avalonia.Media;
using AvaloniaEdit.Document;
using AvaloniaEdit.Rendering;
using CrossCutting.DataStoring.Contract.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.AutoScpWatcher.Avalonia.Models;

namespace UI.AutoScpWatcher.Avalonia.Views
{
    internal class DiffColorizingTransformer : DocumentColorizingTransformer
    {
        public TextFileEntryModel? textFileEntryModel = null;
        protected override void ColorizeLine(DocumentLine line)
        {
            if (!line.IsDeleted)
            {
                int normalizedLineNumer = line.LineNumber - 1;
                if ((textFileEntryModel?.ContentLines != null) && (normalizedLineNumer < textFileEntryModel.ContentLines.Count))
                {
                    string fullLine = CurrentContext.Document.GetText(line.Offset, line.EndOffset - line.Offset);
                    TextFileLine textFileLine = textFileEntryModel.ContentLines[normalizedLineNumer];
                    if (textFileLine.Text.Trim() != fullLine.Trim())
                    {
                        textFileEntryModel.SetLog($"Line mismatch: {textFileLine.Text.Trim()} != {fullLine.Trim()}");
                    }
                    if (textFileLine.Type != TextFileLineType.Unchanged)
                    {
                        Color color = (textFileLine.Type == TextFileLineType.Add) ? Colors.LimeGreen : Colors.OrangeRed;
                        ChangeLinePart(line.Offset, line.EndOffset, (x) => ApplyChanges(x, color));
                    }
                }
            }
        }

        private void ApplyChanges(VisualLineElement element, Color color)
        {
            element.TextRunProperties.ForegroundBrush = new SolidColorBrush(color);
            Typeface typeface = element.TextRunProperties.Typeface;
            element.TextRunProperties.Typeface = new Typeface(typeface.FontFamily, typeface.Style, FontWeight.Bold);
        }
    }
}
