using CrossCutting.DataStoring.Contract.DataClasses;
using Logic.AutoScpManagement;
using Logic.AutoScpManagement.Contract;

if (args.Length == 0)
{
    Console.WriteLine("Give path to settings.json as parameter. See in examples directory.");
    return;
}
Arguments arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<Arguments>(File.ReadAllText(args[0])) ?? throw new Exception();

IAutoScpWatcher autoScpWatcher = new AutoScpWatcher();
autoScpWatcher.FileTransfered += AutoScpWatcher_FileTransfered;
autoScpWatcher.Log += AutoScpWatcher_Log;

Task.Run(async () => await autoScpWatcher.Start(arguments.Directories.First(), new CancellationTokenSource().Token)).Wait();

void AutoScpWatcher_Log(object? sender, string e)
{
    Console.WriteLine(e);
}

void AutoScpWatcher_FileTransfered(object? sender, EventArgs e)
{
    if (arguments.Directories.First().PlaySound)
    {
        Console.Beep();
    }
}