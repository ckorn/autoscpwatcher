﻿using CrossCutting.DataStoring.Contract.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Logic.AutoScpManagement.Contract
{
    public interface IAutoScpWatcher
    {
        event EventHandler FileTransfered;
        event EventHandler<string> Log;
        event EventHandler<IReadOnlyList<TextFileLine>> TextFileLines;
        Task Start(DirectoryEntry directoryEntry, CancellationToken cancellationToken);
        Task Start(TextFileEntry textFileEntry, CancellationToken cancellationToken);
    }
}
