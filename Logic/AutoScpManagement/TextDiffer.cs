﻿using CrossCutting.DataStoring.Contract.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.AutoScpManagement
{
    internal class TextDiffer
    {
        public IReadOnlyList<TextFileLine> GetDiff(string text1, string text2) 
        {
            List<TextFileLine> lines = new();
            List<DiffMatchPatch.Diff> diffList = DiffMatchPatch.DiffMatchPatchModule.Default.DiffMain(text1, text2);
            DiffMatchPatch.DiffMatchPatchModule.Default.DiffCleanupSemantic(diffList);
            foreach (var diff in diffList)
            {
                TextFileLineType lineType = TextFileLineType.Unchanged;
                if (diff.Operation.IsDelete)
                {
                    lineType = TextFileLineType.Remove;
                }
                else if (diff.Operation.IsInsert)
                {
                    lineType = TextFileLineType.Add;
                }
                else if (!diff.Operation.IsEqual)
                {
                    throw new InvalidOperationException(diff.Operation.ToString());
                }
                using StringReader stringReader = new(diff.Text.TrimStart('\r', '\n'));
                string? line = null;
                while ((line = stringReader.ReadLine()) != null)
                {
                    lines.Add(new TextFileLine() { Text = line, Type = lineType });
                }
            }
            return lines;
        }
    }
}
