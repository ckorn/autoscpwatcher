﻿using CrossCutting.DataStoring.Contract.DataClasses;
using Logic.AutoScpManagement.Contract;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace Logic.AutoScpManagement
{
    public class AutoScpWatcher : IAutoScpWatcher
    {
        public event EventHandler<string>? Log;
        public event EventHandler<IReadOnlyList<TextFileLine>>? TextFileLines;
        public event EventHandler? FileTransfered;
        private readonly Dictionary<SftpClient, string> clientHostKeyErrorDictionary = new();
        private readonly TextDiffer textDiffer = new ();

        public async Task Start(TextFileEntry textFileEntry, CancellationToken cancellationToken) 
        {
            OnLog("Start watching");
            ConnectionInfo connectionInfo = new(textFileEntry.Hostname, textFileEntry.Port, textFileEntry.Username,
                new PrivateKeyAuthenticationMethod(textFileEntry.Username, new PrivateKeyFile(textFileEntry.PrivateKeyPath)));
            using SftpClient client = new(connectionInfo);
            clientHostKeyErrorDictionary[client] = string.Empty;
            RegisterHostKeyReceivedEvent(client, textFileEntry.HostKeyFingerprint);
            await ConnectClient(connectionInfo, client, cancellationToken);
            string? lastContent = null;
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    if (!client.IsConnected)
                    {
                        OnLog("Reconnect client");
                        await ConnectClient(connectionInfo, client, cancellationToken);
                    }
                    IReadOnlyList<TextFileLine> contentLines = client.ReadAllLines(textFileEntry.RemotePath)
                        .Select(x => new TextFileLine() { Text = x }).ToList();
                    string content = string.Join(Environment.NewLine, contentLines.Select(x => x.Text));
                    if (lastContent != content) 
                    {
                        if ((lastContent != null) && (textFileEntry.ShowDiff))
                        {
                            contentLines = textDiffer.GetDiff(lastContent, content);
                        }
                        OnLog($"[{DateTime.Now:G}] file changed");
                        OnTextFileLines(contentLines);
                        if (lastContent != null)
                        {
                            OnFileTransfered();
                        }
                        lastContent = content;
                    }
                    else
                    {
                        OnLog($"[{DateTime.Now:G}] no change");
                    }
                }
                catch (Exception e)
                {
                    OnLog("Error on sync. See sync_exceptions.txt");
                    File.AppendAllText("sync_exceptions.txt", GetExceptionText(e) + Environment.NewLine);
                    await Task.Delay(10000, cancellationToken);
                }
                await Task.Delay(textFileEntry.IntervallSeconds * 1000, cancellationToken);
            }
            clientHostKeyErrorDictionary.Remove(client);
        }

        public async Task Start(DirectoryEntry directoryEntry, CancellationToken cancellationToken)
        {
            OnLog("Start watching");
            ConnectionInfo connectionInfo = new(directoryEntry.Hostname, directoryEntry.Port, directoryEntry.Username,
                new PrivateKeyAuthenticationMethod(directoryEntry.Username, new PrivateKeyFile(directoryEntry.PrivateKeyPath)));
            using SftpClient client = new(connectionInfo);
            clientHostKeyErrorDictionary[client] = string.Empty;
            RegisterHostKeyReceivedEvent(client, directoryEntry.HostKeyFingerprint);
            await ConnectClient(connectionInfo, client, cancellationToken);
            while (!cancellationToken.IsCancellationRequested)
            {
                foreach (SyncDirectoryInfo syncDirectoryInfo in directoryEntry.SyncDirectoryInfoList)
                {
                    try
                    {
                        if (!client.IsConnected)
                        {
                            OnLog("Reconnect client");
                            await ConnectClient(connectionInfo, client, cancellationToken);
                        }
                        bool differenceFound = false;
                        IEnumerable<SftpFile> remoteFileList = client.ListDirectory(syncDirectoryInfo.RemotePath);
                        foreach (SftpFile remoteFile in remoteFileList)
                        {
                            if (remoteFile.IsRegularFile)
                            {
                                FileInfo localFile = new(Path.Combine(syncDirectoryInfo.LocalPath, remoteFile.Name));
                                bool notExistLocal = !localFile.Exists;
                                if (notExistLocal)
                                {
                                    differenceFound = true;
                                    OnLog($"New file: {remoteFile.Name}");
                                    OnFileTransfered();
                                    byte[] data = client.ReadAllBytes(Path.Combine(syncDirectoryInfo.RemotePath, remoteFile.Name));
                                    File.WriteAllBytes(localFile.FullName, data);
                                }
                                else if (localFile.Length != remoteFile.Length)
                                {
                                    differenceFound = true;
                                    OnLog($"Updated file: {remoteFile.Name}");
                                    OnFileTransfered();
                                    byte[] data = client.ReadAllBytes(Path.Combine(syncDirectoryInfo.RemotePath, remoteFile.Name));
                                    File.WriteAllBytes(localFile.FullName, data);
                                }
                            }
                        }
                        if (syncDirectoryInfo.DeleteFiles)
                        {
                            HashSet<string> remoteFileNameSet = new(remoteFileList.Where(x => x.IsRegularFile).Select(x => x.Name));
                            foreach (string localFile in Directory.GetFiles(syncDirectoryInfo.LocalPath))
                            {
                                FileInfo localFileInfo = new(localFile);
                                if (!remoteFileNameSet.Contains(localFileInfo.Name))
                                {
                                    differenceFound = true;
                                    OnLog($"Deleted file: {localFileInfo.Name}");
                                    OnFileTransfered();
                                    localFileInfo.Delete();
                                }
                            }
                        }
                        if (!differenceFound)
                        {
                            OnLog("No difference found");
                        }
                    }
                    catch (Exception e)
                    {
                        OnLog("Error on sync. See sync_exceptions.txt");
                        File.AppendAllText("sync_exceptions.txt", GetExceptionText(e) + Environment.NewLine);
                        await Task.Delay(10000, cancellationToken);
                    }
                }
                await Task.Delay(directoryEntry.IntervallSeconds * 1000, cancellationToken);
            }
            clientHostKeyErrorDictionary.Remove(client);
        }

        private void RegisterHostKeyReceivedEvent(SftpClient client, string hostKeyFingerprint)
        {
            client.HostKeyReceived += (sender, e) =>
            {
                string serverFingerprint = ComputeSha256Hash(e.HostKey);
                if (!string.IsNullOrWhiteSpace(hostKeyFingerprint))
                {
                    bool canTrust = (hostKeyFingerprint == $"{e.HostKeyName} {serverFingerprint}");
                    e.CanTrust = canTrust;
                }
                else
                {
                    clientHostKeyErrorDictionary[client] = $"No fingerprint supplied. Fingerprint of server is: {e.HostKeyName} {serverFingerprint}";
                    e.CanTrust = false;
                }
            };
        }

        private async Task ConnectClient(ConnectionInfo connectionInfo, SftpClient client, CancellationToken cancellationToken)
        {
            bool connected = false;
            do
            {
                try
                {
                    clientHostKeyErrorDictionary[client] = string.Empty;
                    client.Connect();
                    connected = true;
                }
                catch (Exception e)
                {
                    string error = "Error on connect.";
                    if (!string.IsNullOrEmpty(clientHostKeyErrorDictionary[client]))
                    {
                        error += " " + clientHostKeyErrorDictionary[client];
                        File.AppendAllText("connect_exceptions.txt", error + Environment.NewLine);
                    }
                    error += " See connect_exceptions.txt";
                    OnLog(error);
                    File.AppendAllText("connect_exceptions.txt", GetExceptionText(e) + Environment.NewLine);
                    await Task.Delay(10000, cancellationToken);
                }
            } while (!connected);
            OnLog($"Connected to {connectionInfo.Host}");
        }

        private static string GetExceptionText(Exception e)
        {
            string text = e.Message;
            text += Environment.NewLine + e.StackTrace;
            if (e.InnerException != null)
            {
                text += Environment.NewLine + GetExceptionText(e.InnerException);
            }
            return text;
        }

        private static string ComputeSha256Hash(byte[] rawData)
        {
            // Create a SHA256
            using SHA256 sha256Hash = SHA256.Create();
            // ComputeHash - returns byte array
            byte[] bytes = sha256Hash.ComputeHash(rawData);

            return Convert.ToBase64String(bytes);
        }

        protected void OnLog(string text) => this.Log?.Invoke(this, text);

        protected void OnTextFileLines(IReadOnlyList<TextFileLine> lines) => this.TextFileLines?.Invoke(this, lines);

        protected void OnFileTransfered() => this.FileTransfered?.Invoke(this, EventArgs.Empty);
    }
}
