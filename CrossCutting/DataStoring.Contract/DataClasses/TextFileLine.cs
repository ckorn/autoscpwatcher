﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.DataStoring.Contract.DataClasses
{
    public class TextFileLine
    {
        public string Text { get; set; } = string.Empty;
        public TextFileLineType Type { get; set; } = TextFileLineType.Unchanged;

        public override string ToString()
        {
            return $"[{Type}]{Text}";
        }
    }
}
