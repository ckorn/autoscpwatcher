﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.DataStoring.Contract.DataClasses
{
    public class Arguments
    {
        public List<DirectoryEntry> Directories { get; set; } = new List<DirectoryEntry>();
        public List<TextFileEntry> TextFiles { get; set; } = new List<TextFileEntry>();
    }
}
