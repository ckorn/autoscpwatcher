﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.DataStoring.Contract.DataClasses
{
    public class TextFileEntry
    {
        public string TabName { get; set; } = "";
        public string Hostname { get; set; } = "";
        public int Port { get; set; } = 22;
        public string Username { get; set; } = "";
        public string PrivateKeyPath { get; set; } = "";
        public string HostKeyFingerprint { get; set; } = "";
        public int IntervallSeconds { get; set; } = 5;
        public bool PlaySound { get; set; } = false;
        public string RemotePath { get; set; } = "";
        public bool ShowDiff { get; set; } = false;
    }
}
