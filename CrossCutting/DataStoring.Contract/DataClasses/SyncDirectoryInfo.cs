﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.DataStoring.Contract.DataClasses
{
    public class SyncDirectoryInfo
    {
        public string LocalPath { get; set; } = "";
        public string RemotePath { get; set; } = "";
        public bool DeleteFiles { get; set; } = false;
    }
}
