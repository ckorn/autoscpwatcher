Watches for changes in a SFTP remote directory and downloads all changes into a local directory.
No local changes are uploaded.

Supply settings.json file as parameter.
See in the example folder.
